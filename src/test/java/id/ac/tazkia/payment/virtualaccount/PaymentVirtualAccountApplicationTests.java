package id.ac.tazkia.payment.virtualaccount;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.ac.tazkia.payment.virtualaccount.dto.VaPayment;

@Import(TestcontainersConfiguration.class)
@SpringBootTest
public class PaymentVirtualAccountApplicationTests {

    @Autowired private ObjectMapper objectMapper;
    @Autowired private PasswordEncoder passwordEncoder;

    @Test
	public void checkConfig() {
        System.out.println("Application runs ok");
    }

    @Test
    public void testSerializeLocalDateTime() throws Exception {
        VaPayment payment = new VaPayment();
        payment.setPaymentTime(LocalDateTime.now());
        System.out.println(objectMapper.writeValueAsString(payment));
    }

    @Test
    public void testGeneratePassword() {
        String hashed = passwordEncoder.encode("test123");
        System.out.println("Hashed : "+hashed);
    }
}
