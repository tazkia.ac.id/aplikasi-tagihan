package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;



@Entity
@Data
public class VirtualAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_tagihan")
    private Tagihan tagihan;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_bank")
    private Bank bank;

    private String nomor;

    @NotNull
    @Enumerated(EnumType.STRING)
    private VaStatus vaStatus = VaStatus.CREATE;

}
