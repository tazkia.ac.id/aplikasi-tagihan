package id.ac.tazkia.payment.virtualaccount.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.payment.virtualaccount.entity.Bank;

public interface BankDao extends JpaRepository<Bank, String> {
    Iterable<Bank> findByIdNotIn(Set<String> ids);
}
