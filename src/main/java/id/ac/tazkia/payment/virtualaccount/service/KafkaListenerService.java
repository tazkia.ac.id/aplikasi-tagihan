package id.ac.tazkia.payment.virtualaccount.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.ac.tazkia.payment.virtualaccount.dao.BankDao;
import id.ac.tazkia.payment.virtualaccount.dao.DebiturDao;
import id.ac.tazkia.payment.virtualaccount.dao.JenisTagihanDao;
import id.ac.tazkia.payment.virtualaccount.dao.KodeBiayaDao;
import id.ac.tazkia.payment.virtualaccount.dao.PembayaranDao;
import id.ac.tazkia.payment.virtualaccount.dao.PeriksaStatusTagihanDao;
import id.ac.tazkia.payment.virtualaccount.dao.TagihanDao;
import id.ac.tazkia.payment.virtualaccount.dao.VirtualAccountDao;
import id.ac.tazkia.payment.virtualaccount.dto.HapusTagihanRequest;
import id.ac.tazkia.payment.virtualaccount.dto.HapusTagihanResponse;
import id.ac.tazkia.payment.virtualaccount.dto.TagihanRequest;
import id.ac.tazkia.payment.virtualaccount.dto.TagihanResponse;
import id.ac.tazkia.payment.virtualaccount.dto.VaPayment;
import id.ac.tazkia.payment.virtualaccount.dto.VaRequestStatus;
import id.ac.tazkia.payment.virtualaccount.dto.VaResponse;
import id.ac.tazkia.payment.virtualaccount.entity.Bank;
import id.ac.tazkia.payment.virtualaccount.entity.Debitur;
import id.ac.tazkia.payment.virtualaccount.entity.JenisPembayaran;
import id.ac.tazkia.payment.virtualaccount.entity.JenisTagihan;
import id.ac.tazkia.payment.virtualaccount.entity.KodeBiaya;
import id.ac.tazkia.payment.virtualaccount.entity.Pembayaran;
import id.ac.tazkia.payment.virtualaccount.entity.PeriksaStatusTagihan;
import id.ac.tazkia.payment.virtualaccount.entity.StatusPembayaran;
import id.ac.tazkia.payment.virtualaccount.entity.StatusPemeriksaanTagihan;
import id.ac.tazkia.payment.virtualaccount.entity.StatusTagihan;
import id.ac.tazkia.payment.virtualaccount.entity.Tagihan;
import id.ac.tazkia.payment.virtualaccount.entity.VaStatus;
import id.ac.tazkia.payment.virtualaccount.entity.VirtualAccount;
import id.ac.tazkia.payment.virtualaccount.exception.InvalidBankException;
import id.ac.tazkia.payment.virtualaccount.exception.InvalidInvoiceException;
import id.ac.tazkia.payment.virtualaccount.exception.InvalidPaymentException;
import jakarta.annotation.PostConstruct;

@Service
@Transactional
public class KafkaListenerService {

    private static final String SUKSES = "sukses";

    private static final String TERIMA_MESSAGE = "Terima message : {}";

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);

    @Value("${kode.biaya.default}")
    private String idKodeBiayaDefault;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private Validator validator;

    @Autowired
    private VirtualAccountDao virtualAccountDao;
    @Autowired
    private BankDao bankDao;
    @Autowired
    private KodeBiayaDao kodeBiayaDao;
    @Autowired
    private TagihanDao tagihanDao;
    @Autowired
    private PembayaranDao pembayaranDao;
    @Autowired
    private DebiturDao debiturDao;
    @Autowired
    private JenisTagihanDao jenisTagihanDao;
    @Autowired
    private PeriksaStatusTagihanDao periksaStatusTagihanDao;
    @Autowired
    private TagihanService tagihanService;
    @Autowired
    private KafkaSenderService kafkaSenderService;

    private KodeBiaya kodeBiayaDefault;

    @PostConstruct
    public void inisialisasiKodeBiaya() {
        LOGGER.debug("ID kode biaya default : {}", idKodeBiayaDefault);
        kodeBiayaDefault = kodeBiayaDao.findById(idKodeBiayaDefault).orElse(new KodeBiaya());
        LOGGER.debug("Kode biaya default : {}", kodeBiayaDefault);
    }

    @KafkaListener(autoStartup = "${kafka.listener.enabled}",
            topics = "${kafka.topic.debitur.request}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleDebiturRequest(String message) {
        Map<String, Object> response = new LinkedHashMap<>();
        try {
            LOGGER.debug(TERIMA_MESSAGE, message);
            Debitur d = objectMapper.readValue(message, Debitur.class);
            BeanPropertyBindingResult binder = new BeanPropertyBindingResult(d, "debitur");
            validator.validate(d, binder);

            if (binder.hasErrors()) {
                LOGGER.warn("Gagal mendaftarkan debitur {}", binder.getAllErrors());
                response.put(SUKSES, false);
                response.put("data", binder.getAllErrors());
                kafkaSenderService.sendDebiturResponse(response);
                return;
            }

            if (debiturDao.findByNomorDebitur(d.getNomorDebitur()) != null) {
                response.put(SUKSES, true);
                response.put("data", "Nomor debitur " + d.getNomorDebitur() + " sudah ada");
                response.put("nomorDebitur", d.getNomorDebitur());
                kafkaSenderService.sendDebiturResponse(response);
                return;
            }

            debiturDao.save(d);
            response.put(SUKSES, true);
            response.put("nomorDebitur", d.getNomorDebitur());
            kafkaSenderService.sendDebiturResponse(response);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            response.put(SUKSES, false);
            response.put("data", err.getMessage());
            kafkaSenderService.sendDebiturResponse(response);
        }
    }

    @KafkaListener(autoStartup = "${kafka.listener.enabled}", topics = "${kafka.topic.tagihan.request}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleTagihanRequest(String message) {
        TagihanResponse response = new TagihanResponse();
        try {
            LOGGER.debug(TERIMA_MESSAGE, message);
            TagihanRequest request = objectMapper.readValue(message, TagihanRequest.class);
            BeanUtils.copyProperties(request, response);

            Tagihan tagihanBaru = new Tagihan();
            tagihanBaru.setPeriodeTagihan(request.getTahunAkademik());

            Debitur d = debiturDao.findByNomorDebitur(request.getDebitur());
            if (d == null) {
                LOGGER.warn("Debitur dengan nomor {} tidak terdaftar", request.getDebitur());
                sendTagihanResponseError(response, "Debitur dengan nomor " + request.getDebitur() + " tidak terdaftar");
                return;
            }
            tagihanBaru.setDebitur(d);

            Optional<JenisTagihan> jt = jenisTagihanDao.findById(request.getJenisTagihan());
            if (!jt.isPresent()) {
                LOGGER.warn("Jenis Tagihan dengan id {} tidak terdaftar", request.getJenisTagihan());
                sendTagihanResponseError(response, "Jenis Tagihan dengan id " + request.getJenisTagihan() + " tidak terdaftar");
                return;
            }
            tagihanBaru.setJenisTagihan(jt.get());

            LOGGER.debug("Kode biaya request : {}", request.getKodeBiaya());
            if (!StringUtils.hasText(request.getKodeBiaya())) {
                tagihanBaru.setKodeBiaya(kodeBiayaDefault);
            } else {
                Optional<KodeBiaya> kodeBiaya = kodeBiayaDao.findById(request.getKodeBiaya());
                if (!kodeBiaya.isPresent()) {
                    LOGGER.warn("Kode biaya dengan id {}  tidak terdaftar", request.getKodeBiaya());
                    tagihanBaru.setKodeBiaya(Optional.of(kodeBiayaDefault).get());
                } else {
                    tagihanBaru.setKodeBiaya(kodeBiaya.get());
                }

            }
            LOGGER.debug("Kode Biaya Tagihan: {}", tagihanBaru.getKodeBiaya());

            tagihanBaru.setNilaiTagihan(request.getNilaiTagihan());
            tagihanBaru.setKeterangan(request.getKeterangan());
            tagihanBaru.setTanggalJatuhTempo(request.getTanggalJatuhTempo());

            if(TagihanRequest.Type.CREATE.equals(request.getJenisRequest())) {
                tagihanService.saveTagihan(tagihanBaru);
            } else if (TagihanRequest.Type.REPLACE.equals(request.getJenisRequest())) {
                String nomorTagihanLama = request.getNomorTagihanLama();
                if (!StringUtils.hasText(nomorTagihanLama)) {
                    LOGGER.warn("Nomor tagihan yang mau direplace tidak boleh null");
                    sendTagihanResponseError(response, "Nomor tagihan yang mau direplace tidak boleh null");
                    return;
                }

                Optional<Tagihan> tagihanLama = tagihanDao.findByNomorAndStatusTagihan(nomorTagihanLama, StatusTagihan.AKTIF);
                if (!tagihanLama.isPresent()) {
                    LOGGER.warn("Nomor tagihan {} tidak ditemukan", nomorTagihanLama);
                    sendTagihanResponseError(response, "Nomor tagihan "+nomorTagihanLama+" tidak ditemukan");
                    return;
                }

                tagihanService.gantiTagihan(tagihanLama.get(), tagihanBaru);

            } else {
                LOGGER.warn("Tagihan Request jenis {} tidak disupport", request.getJenisRequest());
                sendTagihanResponseError(response, "Tagihan Request jenis " + request.getJenisRequest() + " tidak disupport");
            }

        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            response.setSukses(false);
            response.setError(err.getMessage());
            kafkaSenderService.sendTagihanResponse(response);
        }
    }

    private void sendTagihanResponseError(TagihanResponse response, String message) {
        response.setSukses(false);
        response.setError(message);
        kafkaSenderService.sendTagihanResponse(response);
    }

    @KafkaListener(autoStartup = "${kafka.listener.enabled}", topics = "${kafka.topic.va.response}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleVaResponse(String message) {
        try {
            LOGGER.debug(TERIMA_MESSAGE, message);
            VaResponse vaResponse = objectMapper.readValue(message, VaResponse.class);

            VirtualAccount va = virtualAccountDao
                    .findByVaStatusAndTagihanNomorAndBankId(
                            VaStatus.SEDANG_PROSES,
                            vaResponse.getInvoiceNumber(),
                            vaResponse.getBankId());
            if (va == null) {
                LOGGER.warn("VA untuk tagihan dengan nomor {} untuk bank {} tidak ditemukan",
                        vaResponse.getInvoiceNumber(), vaResponse.getBankId());
                return;
            }

            if (VaStatus.INQUIRY.equals(vaResponse.getRequestType())) {
                List<PeriksaStatusTagihan> daftarPeriksaStatus = periksaStatusTagihanDao.findByVirtualAccountAndStatusPemeriksaanTagihan(va, StatusPemeriksaanTagihan.BARU);
                if (daftarPeriksaStatus == null || daftarPeriksaStatus.isEmpty()) {
                    LOGGER.warn("Pemeriksaan status untuk VA {} di bank {} tidak ada", va.getNomor(), va.getBank().getNama());
                    return;
                }

                for (PeriksaStatusTagihan p : daftarPeriksaStatus) {
                    p.setStatusPemeriksaanTagihan(
                            VaRequestStatus.SUCCESS.equals(vaResponse.getRequestStatus())
                            ? StatusPemeriksaanTagihan.SUKSES : StatusPemeriksaanTagihan.ERROR);
                }
            }

            saveVirtualAccountStatus(vaResponse, va);

        } catch (IOException err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

    @KafkaListener(autoStartup = "${kafka.listener.enabled}", topics = "${kafka.topic.va.payment}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleVaPayment(String message) {
        try {
            LOGGER.debug(TERIMA_MESSAGE, message);
            VaPayment payment = objectMapper.readValue(message, VaPayment.class);

            Bank bank = bankDao.findById(payment.getBankId())
                    .orElseThrow(() -> new InvalidBankException("Bank Id "+payment.getBankId()+" tidak terdaftar"));

            Tagihan tagihan = tagihanDao.findByNomorAndStatusTagihan(payment.getInvoiceNumber(), StatusTagihan.AKTIF)
                    .orElseThrow(() -> new InvalidInvoiceException("Invoice # " + payment.getInvoiceNumber() + " tidak terdaftar/tidak aktif"));

            List<VirtualAccount> daftarVa = virtualAccountDao.findByVaStatusAndTagihanNomor(VaStatus.AKTIF, tagihan.getNomor());

            BigDecimal akumulasiPembayaran = tagihan.getJumlahPembayaran().add(payment.getAmount());

            validasiPayment(tagihan, daftarVa, payment);

            if (pembayaranKurangDariTagihan(akumulasiPembayaran, tagihan)) {
                tagihan.setStatusPembayaran(StatusPembayaran.DIBAYAR_SEBAGIAN);
            } else {
                tagihan.setStatusPembayaran(StatusPembayaran.LUNAS);
                tagihan.setStatusTagihan(StatusTagihan.NONAKTIF);
            }
            tagihan.setJumlahPembayaran(akumulasiPembayaran);

            // update VA
            Pembayaran p = simpanPembayaran(bank, tagihan, daftarVa, payment);

            tagihanDao.save(tagihan);

            LOGGER.info("Pembayaran melalui VA Bank {} Nomor {} telah diterima",
                    bank.getNama(),
                    payment.getAccountNumber());

            kafkaSenderService.sendNotifikasiPembayaran(p);
        } catch (IOException | InvalidBankException | InvalidInvoiceException | InvalidPaymentException err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

    @KafkaListener(autoStartup = "${kafka.listener.enabled}",
            topics = "${kafka.topic.tagihan.hapus.request}",
            groupId = "${spring.kafka.consumer.group-id}")
    public void handleHapusTagihanRequest(String message) {
        LOGGER.debug(TERIMA_MESSAGE, message);
        HapusTagihanResponse response = new HapusTagihanResponse();

        try {
            HapusTagihanRequest request = objectMapper.readValue(message, HapusTagihanRequest.class);
            LOGGER.info("Hapus tagihan {} - {} - {}", request.getNomorTagihan(), request.getJenisTagihan(), request.getDebitur());
            BeanUtils.copyProperties(request, response);
            Optional<Tagihan> tagihan = tagihanDao.findByNomorAndStatusTagihan(request.getNomorTagihan(), StatusTagihan.AKTIF);
            if (!tagihan.isPresent()) {
                response.setSukses(false);
                response.setKeterangan("Tagihan nomor "+request.getNomorTagihan()+" tidak ada di database");
                kafkaSenderService.sendHapusTagihanResponse(response);
                return;
            }
            Tagihan tag = tagihan.get();
            tagihanService.hapusTagihan(tag);
            response.setNilai(tag.getNilaiTagihan());
            response.setSukses(true);
            response.setKeterangan("Tagihan "+request.getNomorTagihan()+" berhasil dihapus");
            kafkaSenderService.sendHapusTagihanResponse(response);
        } catch (JsonProcessingException e) {
            LOGGER.warn(e.getMessage(), e);
            response.setSukses(false);
            response.setKeterangan(e.getMessage());
            kafkaSenderService.sendHapusTagihanResponse(response);
        }
    }

    private static boolean pembayaranKurangDariTagihan(BigDecimal akumulasiPembayaran, Tagihan tagihan) {
        return akumulasiPembayaran.compareTo(tagihan.getNilaiTagihan()) < 0;
    }

    private static boolean pembayaranMelebihiTagihan(BigDecimal akumulasiPembayaran, Tagihan tagihan) {
        return akumulasiPembayaran.compareTo(tagihan.getNilaiTagihan()) > 0;
    }

    private void saveVirtualAccountStatus(VaResponse vaResponse, VirtualAccount va) {
        if (VaRequestStatus.ERROR.equals(vaResponse.getRequestStatus())) {
            va.setVaStatus(VaStatus.ERROR);
            virtualAccountDao.save(va);
        }

        if (VaStatus.DELETE.equals(vaResponse.getRequestType())) {
            va.setVaStatus(VaStatus.NONAKTIF);
            virtualAccountDao.save(va);
        }

        va.setNomor(vaResponse.getAccountNumber());
        va.setVaStatus(VaStatus.AKTIF);
        virtualAccountDao.save(va);
    }

    private Pembayaran simpanPembayaran(Bank bank, Tagihan tagihan, List<VirtualAccount> daftarVa, VaPayment payment) throws InvalidPaymentException {

        VirtualAccount vaPembayaran = null;
        for (VirtualAccount va : daftarVa) {
            if (bank.getId().equalsIgnoreCase(va.getBank().getId())) {
                vaPembayaran = va;
                va.setVaStatus(StatusPembayaran.LUNAS.equals(tagihan.getStatusPembayaran())
                        ? VaStatus.NONAKTIF : VaStatus.UPDATE);
            } else {
                va.setVaStatus(StatusPembayaran.LUNAS.equals(tagihan.getStatusPembayaran())
                        ? VaStatus.DELETE : VaStatus.UPDATE);
            }
            virtualAccountDao.save(va);
        }

        if (vaPembayaran == null) {
            throw new InvalidPaymentException("Virtual account untuk nomor tagihan "+tagihan.getNomor()+" dan bank "+bank.getNama()+" tidak terdaftar");
        }

        Pembayaran p = new Pembayaran();
        p.setBank(bank);
        p.setTagihan(tagihan);
        p.setJenisPembayaran(JenisPembayaran.VIRTUAL_ACCOUNT);
        p.setVirtualAccount(vaPembayaran);
        p.setJumlah(payment.getAmount());
        p.setReferensi(payment.getReference());
        p.setKeterangan("Pembayaran melalui VA Bank " + bank.getNama() + " Nomor " + payment.getAccountNumber());
        p.setWaktuTransaksi(payment.getPaymentTime());
        pembayaranDao.save(p);

        return p;
    }

    private void validasiPayment(Tagihan tagihan, List<VirtualAccount> daftarVa, VaPayment payment) throws InvalidInvoiceException {

        if (StatusPembayaran.LUNAS.equals(tagihan.getStatusPembayaran())) {
            throw new InvalidInvoiceException("Invoice # " + payment.getInvoiceNumber() + " sudah lunas");
        }

        if (daftarVa == null || daftarVa.isEmpty()) {
            throw new InvalidInvoiceException("Invoice # " + payment.getInvoiceNumber() + " tidak memiliki va");
        }
        
        BigDecimal akumulasiPembayaran = tagihan.getJumlahPembayaran().add(payment.getAmount());
        if (pembayaranMelebihiTagihan(akumulasiPembayaran, tagihan)) {
            new InvalidInvoiceException("Invoice # " + payment.getInvoiceNumber()
                    + " nilai pembayaran [" + akumulasiPembayaran
                    + "] melebihi nilai tagihan [" + tagihan.getNilaiTagihan() + "]");
        }
        
    }
}
