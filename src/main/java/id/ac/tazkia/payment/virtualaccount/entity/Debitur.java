package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Entity @Data
public class Debitur {
    @Id @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NotNull @NotEmpty
    @Column(unique = true)
    private String nomorDebitur;

    @NotNull @NotEmpty
    private String nama;

    @Email
    private String email;
    private String noHp;
    
}
