package id.ac.tazkia.payment.virtualaccount.dao;

import id.ac.tazkia.payment.virtualaccount.entity.ResendTagihan;
import org.springframework.data.repository.CrudRepository;

public interface ResendTagihanDao extends CrudRepository<ResendTagihan, String> {

}
