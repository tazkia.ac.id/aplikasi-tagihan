package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;



@Entity @Data
public class BulkDeleteTagihan {

    @Id
    private String id;
    private String nomorDebitur;

    @ManyToOne @JoinColumn(name = "id_jenis_tagihan")
    private JenisTagihan jenisTagihan;

    @Column(name = "delete_status")
    @Enumerated(EnumType.STRING)
    private StatusDeleteTagihan statusDeleteTagihan;
}
