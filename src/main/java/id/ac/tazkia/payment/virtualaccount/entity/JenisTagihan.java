package id.ac.tazkia.payment.virtualaccount.entity;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity @Data
public class JenisTagihan {
    @Id @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NotNull @NotEmpty
    @Column(unique = true)
    private String kode;

    @NotNull @NotEmpty @Column(unique = true)
    private String nama;

    @NotNull @Enumerated(EnumType.STRING)
    private TipePembayaran tipePembayaran;

    @NotNull
    private Boolean aktif = Boolean.FALSE;

    @ManyToMany
    @JoinTable(
            name = "jenis_tagihan_bank",
            joinColumns = @JoinColumn(name = "id_jenis_tagihan"),
            inverseJoinColumns = @JoinColumn(name = "id_bank")
    )
    private Set<Bank> daftarBank = new HashSet<>();
}
