package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Entity @Data
public class Bank {
    @Id @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @NotNull @NotEmpty
    @Column(unique = true)
    private String kode;
    @NotNull @NotEmpty
    private String nama;
    @NotNull @NotEmpty
    private String nomorRekening;
    @NotNull @NotEmpty
    private String namaRekening;
    @NotNull @Min(0)
    private Integer jumlahDigitVirtualAccount = 0;
    @NotNull @Min(0)
    private Integer jumlahDigitPrefix = 0;
}
