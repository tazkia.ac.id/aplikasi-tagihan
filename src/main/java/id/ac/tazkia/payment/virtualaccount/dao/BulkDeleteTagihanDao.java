package id.ac.tazkia.payment.virtualaccount.dao;

import id.ac.tazkia.payment.virtualaccount.entity.BulkDeleteTagihan;
import org.springframework.data.repository.CrudRepository;

public interface BulkDeleteTagihanDao extends CrudRepository<BulkDeleteTagihan, String> {
}
