package id.ac.tazkia.payment.virtualaccount.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity @Data
public class Tagihan {

    @Id @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NotNull @NotEmpty
    private String nomor;

    private String periodeTagihan;

    @ManyToOne @NotNull
    @JoinColumn(name = "id_debitur")
    private Debitur debitur;

    @ManyToOne @NotNull
    @JoinColumn(name = "id_jenis_tagihan")
    private JenisTagihan jenisTagihan;

    @ManyToOne @NotNull
    @JoinColumn(name = "id_kode_biaya")
    private KodeBiaya kodeBiaya;

    @NotNull @Min(0)
    private BigDecimal nilaiTagihan;

    @NotNull @Min(0)
    private BigDecimal jumlahPembayaran = BigDecimal.ZERO;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private LocalDate tanggalJatuhTempo;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private LocalDate tanggalTagihan = LocalDate.now();

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    private String keterangan;

    @NotNull @Enumerated(EnumType.STRING)
    private StatusPembayaran statusPembayaran = StatusPembayaran.BELUM_DIBAYAR;

    @NotNull @Enumerated(EnumType.STRING)
    private StatusTagihan statusTagihan = StatusTagihan.AKTIF;

    @NotNull @Enumerated(EnumType.STRING)
    private StatusNotifikasi statusNotifikasi = StatusNotifikasi.BELUM_TERKIRIM;

    public BigDecimal getNilaiTagihanEfektif() {
        return nilaiTagihan.subtract(jumlahPembayaran);
    }
}
