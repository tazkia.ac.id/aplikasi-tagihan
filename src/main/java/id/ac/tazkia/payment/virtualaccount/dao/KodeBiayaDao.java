package id.ac.tazkia.payment.virtualaccount.dao;

import id.ac.tazkia.payment.virtualaccount.entity.KodeBiaya;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KodeBiayaDao extends JpaRepository<KodeBiaya, String> {
}
