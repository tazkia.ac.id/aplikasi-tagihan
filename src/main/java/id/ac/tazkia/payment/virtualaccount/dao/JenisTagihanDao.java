package id.ac.tazkia.payment.virtualaccount.dao;

import id.ac.tazkia.payment.virtualaccount.entity.JenisTagihan;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JenisTagihanDao extends JpaRepository<JenisTagihan, String> {
    List<JenisTagihan> findByAktifOrderByKode(Boolean aktif);
}
