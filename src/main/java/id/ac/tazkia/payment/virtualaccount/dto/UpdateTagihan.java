package id.ac.tazkia.payment.virtualaccount.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateTagihan {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull @Future
    private LocalDate tanggalJatuhTempo;

    @NotNull @Min(1)
    private BigDecimal nilaiTagihan;
}
