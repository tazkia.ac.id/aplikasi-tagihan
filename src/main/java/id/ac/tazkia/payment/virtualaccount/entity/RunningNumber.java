package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Entity @Data
public class RunningNumber {
    public static final String PEMAKAIAN_DEFAULT = "DEFAULT";

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NotNull @NotEmpty
    private String prefix;

    @NotNull @NotEmpty
    private String pemakaian = PEMAKAIAN_DEFAULT;

    @NotNull @Min(0)
    private Long lastNumber = 0L;
}
