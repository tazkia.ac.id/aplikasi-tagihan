package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import lombok.Data;



@Entity @Data
public class ResendPembayaran {
    @Id @Column(name = "id_pembayaran")
    private String id;

    @OneToOne @MapsId @JoinColumn(name = "id_pembayaran")
    private Pembayaran pembayaran;
}
