package id.ac.tazkia.payment.virtualaccount.dao;

import id.ac.tazkia.payment.virtualaccount.entity.Debitur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DebiturDao extends JpaRepository<Debitur, String> {
    Debitur findByNomorDebitur(String nomor);
    Page<Debitur> findByNomorDebiturOrNamaContainingIgnoreCase(String nomor, String nama, Pageable page);
}
