package id.ac.tazkia.payment.virtualaccount.dto;

import lombok.Data;

@Data
public class HapusTagihanRequest {
    private String jenisTagihan;
    private String kodeBiaya;
    private String debitur;
    private String nomorTagihan;
}
