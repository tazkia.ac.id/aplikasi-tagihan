package id.ac.tazkia.payment.virtualaccount.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data @Entity
public class KodeBiaya {

    @Id @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NotEmpty
    private String kode;

    @NotEmpty
    private String nama;
}
